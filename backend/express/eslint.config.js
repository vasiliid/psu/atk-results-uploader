import js from '@eslint/js';

export default [
  {
    root: true,
    env: {
      browser: true,
      es2023: true
    },
    linterOptions: {
      noInlineConfig: true
    },
    parserOptions: {
      ecmaVersion: 'es2023',
      sourceType: 'module'
    },
    ignorePatterns: ['**/public/**', '**/dist/**'],
    extends: [
      'eslint:recommended',
      'prettier'
    ],
    rules: {
      ...js.configs.recommended.rules
    }
  }
];
