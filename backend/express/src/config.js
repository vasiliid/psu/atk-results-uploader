// src/config.js

import dotenv from "dotenv";

const nodeEnv = process.env.NODE_ENV || "development";
dotenv.config({ path: `.env.${nodeEnv}` });

export const config = {
  appHost: process.env.APP_HOST,
  appPort: process.env.APP_PORT,
  dbHost: process.env.DB_HOST,
  dbName: process.env.DB_NAME,
  dbUsername: process.env.DB_USERNAME,
  dbPassword: process.env.DB_PASSWORD,
};
