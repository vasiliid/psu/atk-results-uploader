// src/routes/router.js

import express from "express";
import * as userController from "../controllers/usersController.js";
import * as resultsController from "../controllers/resultsController.js";

const router = express.Router();

// API Ping route
router.get("/ping", (req, res) => {
  res.send("pong");
});

router.post("/user/register", userController.registerUser);
router.post("/user/login", userController.loginUser);
// router.get("/user/profile/:userId", userController.getUserProfile);

router.post("/results/upload", resultsController.uploadResult);
router.get("/results/user/:userId", resultsController.showResults);
router.get("/results/all", resultsController.showAllResults);

export default router;
