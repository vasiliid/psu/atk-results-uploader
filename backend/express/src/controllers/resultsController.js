// src/controllers/resultsController.js

import { config } from "../config.js";
import mysql from "mysql2/promise";

// Creating a pool for MySQL connections
const pool = mysql.createPool({
  host: config.dbHost,
  database: config.dbName,
  user: config.dbUsername,
  password: config.dbPassword,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

export const uploadResult = async (req, res) => {
  try {
    const { userId, date, result, resultImage, status = 0 } = req.body;

    console.log(date);

    const [newResult] = await pool.query(
      `INSERT INTO results (userId, date, result, resultImage, status)
    VALUES (?, ?, ?, ?, ?)`,
      [userId, date, result, resultImage, status],
    );

    res.status(201).json({
      message: "Result uploaded successfully",
      data: {
        userId,
        date,
        result,
        resultImage,
        status,
      },
    });
  } catch (error) {
    console.error("Upload error:", error);
    res.status(500).json({ message: "Failed to upload result" });
  }
};

export const showResults = async (req, res) => {
  try {
    const { userId } = req.params;

    const sql = userId ? `SELECT * FROM results WHERE userId = ?` : `SELECT * FROM results`;

    const [results] = await pool.query(sql, userId ? [userId] : []);

    res.status(200).json({
      message: "Results fetched successfully",
      data: results,
    });
  } catch (error) {
    console.error("Fetch error:", error);
    res.status(500).json({ message: "Failed to fetch results" });
  }
};

export const showAllResults = async (req, res) => {
  try {
    const [results] = await pool.query(
      `SELECT results.id, users.name, users.studentId, results.date, results.result, results.resultImage, results.status FROM results JOIN users ON results.userId = users.id ORDER BY results.id`,
    );

    res.status(200).json({
      message: "All results fetched successfully",
      data: results,
    });
  } catch (error) {
    console.error("Fetch error:", error);
    res.status(500).json({ message: "Failed to fetch all results" });
  }
};
