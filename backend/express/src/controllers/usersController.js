// src/controllers/usersController.js

import { config } from "../config.js";
import mysql from "mysql2/promise";
import bcrypt from "bcrypt";

// Creating a pool for MySQL connections
const pool = mysql.createPool({
  host: config.dbHost,
  database: config.dbName,
  user: config.dbUsername,
  password: config.dbPassword,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

export const registerUser = async (req, res) => {
  try {
    const { email, name, studentId, password, admin = false } = req.body;

    // Hash password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    // Create new user SQL query
    const [result] = await pool.query(
      `INSERT INTO users (email, name, studentId, password, admin) VALUES (?, ?, ?, ?, ?)`,
      [email, name, studentId, hashedPassword, admin],
    );

    const [users] = await pool.query(`SELECT id, email, name, studentId, admin FROM users WHERE email = ?`, [email]);
    const user = users[0];
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    res.status(201).json({
      message: "User registered successfully",
      user: {
        id: user.id,
        email: user.email,
        name: user.name,
        studentId: user.studentId,
        isAdmin: user.admin,
      },
    });
  } catch (error) {
    console.error("Registration error:", error);
    res.status(500).json({ message: "Failed to register user" });
  }
};

export const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Find user by email SQL query
    const [users] = await pool.query(
      `SELECT id, email, name, studentId, password as passwordHash, admin FROM users WHERE email = ?`,
      [email],
    );
    const user = users[0];
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Check password
    const isMatch = await bcrypt.compare(password, user.passwordHash);
    if (!isMatch) {
      return res.status(400).json({ message: "Invalid credentials" });
    }

    // Login successful
    res.json({
      message: "User logged in successfully",
      user: {
        id: user.id,
        email: user.email,
        name: user.name,
        studentId: user.studentId,
        isAdmin: user.admin,
      },
    });
  } catch (error) {
    console.error("Login error:", error);
    res.status(500).json({ message: "Failed to log in user" });
  }
};
