// src/app.js

import express from "express";

import router from "./routes/router.js";

// Express server
const app = express();
app.use(express.json());
app.set("x-powered-by", false);
app.set("etag", false);
app.set("trust proxy", ["loopback", "linklocal", "uniquelocal"]);

// For all requests
app.use((req, res, next) => {
  // CORS headers
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
  res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Time in logs
  const appLocale = "ru-RU";
  const appTimezone = "Asia/Bangkok";
  const d = new Date();
  const lt = d.toLocaleTimeString(appLocale, {
    timeZone: appTimezone,
    // timeZoneName: "short",
  });

  console.log(lt + " " + req.method + " " + req.url);
  next();
});

// Route only requests that start with /api
app.use("/api", router);

// Docker Healthcheck ping route
// app.get("/ping", (req, res) => {
//   res.send("pong");
// });

// Catch all wrong requests
app.use((req, res) => {
  res.status(404).send("nope");
});

export default app;
