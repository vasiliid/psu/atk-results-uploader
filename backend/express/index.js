// index.js

import app from "./src/app.js";
import { config } from "./src/config.js";

app.listen(config.appPort, config.appHost, () => {
  console.log(`App listening at http://${config.appHost}:${config.appPort}`);
});