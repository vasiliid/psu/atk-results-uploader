import js from '@eslint/js';

export default [
  {
    root: true,
    env: {
      browser: true,
      es2023: true,
    },
    linterOptions: {
      noInlineConfig: true,
    },
    parserOptions: {
      ecmaVersion: 'es2023',
      sourceType: 'module',
    },
    ignorePatterns: ['**/public/**', '**/dist/**'],
    extends: [
      'eslint:recommended',
      'plugin:vue/vue3-recommended',
      '@vue/eslint-config-prettier/skip-formatting',
      'prettier',
    ],
    rules: {
      ...js.configs.recommended.rules,
      'vue/multi-word-component-names': 'off',
      'vue/no-reserved-component-names': 'off',
    },
  },
];
