import { createRouter, createWebHistory } from 'vue-router';
import { useUserStore } from '@/stores/userStore'; // Import your user store

import HomePage from '@/views/HomePage.vue';

const routes = [
  { path: '/', name: 'home', component: HomePage },
  { path: '/login', component: () => import('@/views/LoginPage.vue') },
  { path: '/register', component: () => import('@/views/RegisterPage.vue') },
  {
    path: '/upload',
    component: () => import('@/views/UploadPage.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/status',
    component: () => import('@/views/StatusPage.vue'),
    meta: { requiresAuth: true },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const userStore = useUserStore(); // Access the store inside the navigation guard
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!userStore.isLoggedIn) {
      // Redirect to the login page if not logged in
      next({ path: '/login' });
    } else {
      // Proceed to the route if logged in
      next();
    }
  } else {
    // If no auth is required, proceed as normal
    next();
  }
});

export default router;
