// src/main.js

import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import router from './router';
import { useUserStore } from './stores/userStore';

import PrimeVue from 'primevue/config';

import Button from 'primevue/button';
import Ripple from 'primevue/ripple';
import StyleClass from 'primevue/styleclass';
import InputText from 'primevue/inputtext';
import Textarea from 'primevue/textarea';
import FileUpload from 'primevue/fileupload';
import RadioButton from 'primevue/radiobutton';
import Calendar from 'primevue/calendar';

import './assets/css/bootstrap-reboot.css';
import './assets/css/main.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import 'primevue/resources/themes/lara-light-blue/theme.css';
import 'primevue/resources/primevue.min.css';

const app = createApp(App);

app.use(createPinia());

const userStore = useUserStore();
userStore.init();

app.use(PrimeVue);
app.use(router);

app.component('Button', Button);
app.component('InputText', InputText);
app.component('Textarea', Textarea);
app.component('FileUpload', FileUpload);
app.component('RadioButton', RadioButton);
app.component('Calendar', Calendar);

app.directive('ripple', Ripple);
app.directive('styleclass', StyleClass);

app.mount('#app');
