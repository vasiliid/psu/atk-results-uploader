// src/stores/userStore.js

import { defineStore } from 'pinia';
import axios from 'axios';
import Cookies from 'js-cookie';

export const useUserStore = defineStore('user', {
  state: () => ({
    userData: null,
    isLoggedIn: false,
  }),
  actions: {
    init() {
      const userDataCookie = Cookies.get('userData');
      const isLoggedInCookie = Cookies.get('isLoggedIn');

      if (userDataCookie && isLoggedInCookie === 'true') {
        this.userData = JSON.parse(userDataCookie);
        this.isLoggedIn = true;
      }
    },
    async login(email, password) {
      const apiUrl = __API_URL__;
      try {
        const response = await axios.post(`${apiUrl}/api/user/login`, { email, password });
        this.userData = response.data;
        this.isLoggedIn = true;
        Cookies.set('userData', JSON.stringify(this.userData), { expires: 7 });
        Cookies.set('isLoggedIn', 'true', { expires: 7 });
      } catch (error) {
        console.error('Login failed:', error);
        throw error;
      }
    },
    async register(email, name, studentId, password) {
      const apiUrl = __API_URL__;
      try {
        const response = await axios.post(`${apiUrl}/api/user/register`, { email, name, studentId, password });
        this.userData = response.data;
        this.isLoggedIn = true;
        Cookies.set('userData', JSON.stringify(this.userData), { expires: 7 });
        Cookies.set('isLoggedIn', 'true', { expires: 7 });
      } catch (error) {
        console.error('Registration failed:', error);
        throw error;
      }
    },
    async logout() {
      this.userData = null;
      this.isLoggedIn = false;
      Cookies.remove('userData');
      Cookies.remove('isLoggedIn');
    },
  },
});
