// src/stores/resultsStore.js

import { defineStore } from 'pinia';
import axios from 'axios';

export const useResultsStore = defineStore('results', {
  state: () => ({
    results: [],
  }),
  actions: {
    async fetchAllResults() {
      const apiUrl = __API_URL__;
      try {
        const response = await axios.get(`${apiUrl}/api/results/all`);
        this.results = response.data.data;
        // console.log(response.data.data);
      } catch (error) {
        console.error('Failed to fetch results:', error);
      }
    },

    async uploadResult(resultData) {
      const apiUrl = __API_URL__;
      try {
        const response = await axios.post(`${apiUrl}/api/results/upload`, resultData, {
          headers: {
            'Content-Type': 'application/json',
          },
        });
        this.results.push(response.data);
      } catch (error) {
        console.error('Failed to upload result:', error);
        throw error;
      }
    },
  },
});
